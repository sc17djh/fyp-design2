import sqlite3
import sys
import json
from datetime import datetime

import time

class DatabaseError(Exception):
  pass 

class Database():

  def __init__(self, database_name):
    '''
    Initialise the database connection and cursor
    '''
    try:
      # setup connection
      self._db_connection = sqlite3.connect(database_name)
      self._cursor = self._db_connection.cursor()

      # print version
      print('Connecting to SQLite database...')
      self._cursor.execute('SELECT SQLITE_VERSION()')
      db_version = self._cursor.fetchone()

      self.initialise()

    except Exception as err:
        print('Error: connection could not be established {}'.format(err))
        sys.exit(-1)
    else:
      print('Connection established\n{}'.format(db_version[0]))


  def initialise(self):
    '''
    Setup the database tables
    '''
    self.create_users_table()


    credentials = [
        ['U001', 'doctor1', 'password1', {"uid": "U001", "role": "doctor", "hospital": "H001"}],
        ['U002', 'pharmacist1', 'password1', {"uid": "U002", "role": "pharmacist"}],
        ['U003', 'researcher1', 'password1', {"uid": "U003", "role": "researcher"}],
        ['U004', 'paramedic1', 'password1', {"uid": "U004", "role": "paramedic", "emergency": "true"}],
        ['U005', 'generic1', 'password1', {"uid": "U005"}],
        
    ]

    self.populate_users_table(credentials)


  def __del__(self):
    self._cursor.close()
    self._db_connection.close()


  def _query(self, query, params=None):
    try:
      self._log("query '{}', params '{}'\n".format(query, params))
      if(params):
        self._cursor.execute(query, params)
      else:
        self._cursor.execute(query)
    except Exception as err:
      self._log("ERROR: Exception on query '{}', params: '{}',\nerror: '{}'\n".format(query, params, err))
      # re-raise error to be caught further up incase of rollback requirements
      raise DatabaseError(err)


  def _log(self, message):
    print("{} : DB-LOG : {}".format(datetime.now(), message))


####################
### TEST METHODS ###
####################

  def create_users_table(self):
    '''
    Create the users table.
    '''
    SQL = "CREATE TABLE users (\
            user_id           TEXT PRIMARY KEY NOT NULL,\
            username          TEXT UNIQUE NOT NULL,\
            password          TEXT NOT NULL,\
            attributes        TEXT NOT NULL\
          )"
    # try deleting the table first to prevent errors
    self.delete_users_table()

    try:
      self._query(SQL)
      self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)
    

  def delete_users_table(self):
    '''
    Delete the users table.
    '''
    SQL = "DROP TABLE IF EXISTS users"

    try:
        self._query(SQL)
        self._db_connection.commit() 
    except DatabaseError as err:
      # rollback to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back DB")
      raise(DatabaseError(err))


  def populate_users_table(self, credentials_list):
    for credentials in credentials_list:
      try:
        self.create_user(*credentials)
      except Exception as err:
        print("Failed to create user in test population,\n{}".format(err))
    print("Populated test users table")


####################
### USER METHODS ###
####################

  def create_user(self, user_id, username, password, attributes):
    SQL = "INSERT INTO users (user_id, username, password, attributes)\
          VALUES (?, ?, ?, ?)"

    try:
        self._query(SQL, (user_id, username, password, json.dumps(attributes)))
        self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)


  def delete_user(self, user_id):
    SQL = "DELETE FROM users WHERE user_id==?"

    try:
        self._query(SQL, (user_id,))
        self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)


  def update_user_attributes(self, user_id, new_attributes):
    SQL = "UPDATE users SET attributes=? WHERE user_id==?"

    try:
        self._query(SQL, (json.dumps(new_attributes), user_id))
        self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)

  
  def get_user_attributes(self, user_id):
    SQL = "SELECT attributes FROM users WHERE user_id == ?"

    try:
      self._query(SQL, (user_id,))
      results = self._cursor.fetchone()
      if(results):
        return(results)
      else:
        return(None)
    except Exception as err:
      raise DatabaseError(err)


  def get_user(self, username):
    SQL = "SELECT * FROM users WHERE username == ?"

    try:
      self._query(SQL, (username,))
      results = self._cursor.fetchone()
      if(results):
        return(results)
      else:
        return(None)
    except Exception as err:
      raise DatabaseError(err)


  def get_user_list(self):
    SQL = "SELECT * FROM users"

    try:
      self._query(SQL)
      results = self._cursor.fetchall()
      if(len(results) == 0):
        return(None)
      else:
        return(results)
    except Exception as err:
      raise DatabaseError(err)


