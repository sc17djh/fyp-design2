from pydantic import BaseModel

'''
models.py stores the request body models for use with fastAPI. models correspond to a model of the input body data supplied to the API endpoint.
'''

class Record(BaseModel):
    patient_id: str
    record_type: str 
    desc: str

class Record_Pair(BaseModel):
    patient_id: str
    record_id: str

class User_Attributes(BaseModel):
    user_id: str
    attributes: str

class User(BaseModel):
    user_id: str
    username: str
    password: str
    attributes: str

class Key(BaseModel):
    patient_id: str
    record_id: str
    key: str
    record_type: str
