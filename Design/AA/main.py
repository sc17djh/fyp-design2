from fastapi import FastAPI, Header, HTTPException, File, UploadFile, Form, Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import serialization, hashes
import time
import secrets
import hashlib
import requests
import json
import sys

from config import *
import dbHooks
import models 


app = FastAPI()
db = dbHooks.Database("attributes.db")

security = HTTPBasic()

# load private key
key_file = open("private-key", "rb")
private_key = serialization.load_pem_private_key(
    key_file.read(),
    password=None,
    backend=default_backend()
)  
key_file.close()


def generate_rsa_key_pair():
    '''
    Generate RSA public/private key pair and save to filesystem
    '''
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )

    pem_priv = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )
    
    public_key = private_key.public_key()
    pem_pub = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )

    # save keys to files
    with open("private-key", "wb") as f:
        f.write(pem_priv)
    
    with open("public-key", "wb") as f:
        f.write(pem_pub)


@app.get("/cert", status_code=200)
async def get_cert(credentials: HTTPBasicCredentials = Depends(security)):
    start_time = time.time()
    user_id, attributes = authenticate_user(credentials)

    # sign attributes
    signature = private_key.sign(
        bytes(attributes, 'utf-8'),
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA256()
    )

    # generate certificate
    cert = {"uid": user_id, "attributes": attributes, "signature": signature.hex()}

    computation_time = time.time() - start_time
    
    return(json.dumps(cert))


# test endpoint
@app.post("/user", status_code=201)
async def create_user(user: models.User):
    db.create_user(user.user_id, user.username, user.password, user.attributes)

    return({'message': 'created'})


# test endpoint
@app.put("/user", status_code=200)
async def update_user(user: models.User_Attributes):
    db.update_user_attributes(user.user_id, user.new_attributes)

    return({'message': 'updated'})


def authenticate_user(credentials: HTTPBasicCredentials):
    '''
    Authenticate the user using HTTP Basic Auth
    '''
    error = HTTPException(
            status_code = 401,
            detail = "Incorrect username or password"
        )

    # check user credentials
    if(credentials is None):
        raise error

    user = db.get_user(credentials.username)
    password_correct = False

    if(user):
        # check password matches
        correct_password = user[2]
        password_correct = secrets.compare_digest(correct_password, credentials.password)

    if not(user and password_correct):
        raise error

    # return user id and attributes
    return((user[0], user[3]))


def revoke_user_privileges(user_id, new_attributes = None):
    '''
    Revoke a users attributes completely or use a new set of attributes
    '''
    start_time = time.time()

    # revoke users previous certificates by sending message to TA containing certificate hash and user_id
    attributes = db.get_user_attributes(user_id)[0]
    m = hashlib.sha256()
    m.update(bytes(attributes, 'utf-8'))
    m.update(bytes('revoked', 'utf-8'))
    hashed_attributes = m.digest()


    # sign revoked cert
    signature = private_key.sign(
        hashed_attributes,
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA256()
    )

    # generate revoked cert
    cert = {"user_id": user_id, "attribute_hash": hashed_attributes.hex(), "signature": signature.hex()}

    # send revoked cert to TA
    response = revoke_certificate(user_id, json.dumps(cert))
    response.raise_for_status()

    # delete user account
    db.delete_user(user_id)

    end_time = time.time()
    return(end_time - start_time)


def revoke_certificate(user_id, cert):
    url = "{}/admin/revoke".format(KEY_SERVER_BASE_URL)
    headers = {'Content-Type': 'application/json'}
    body = {'user_id': user_id,
            'revoked_cert': cert}

    response = requests.put(url, headers=headers, json=body)
    return(response)


def evaluate_revocation_time(revoked_user_num):
    '''
    Evaluate how revocation of user privileges scales with number of system users
    '''
    credentials = []
    for x in range(revoked_user_num):
        credentials.append(['U01{}'.format(x), 'doctor1{}'.format(x), 'password1', [{'uid': 'U01{}'.format(x), 'role': 'doctor'}]])

    db.populate_users_table(credentials)

    times = []
    for i in range(revoked_user_num):
        if(i == 1 or i % 5 == 0):
            times.append(revoke_user_privileges('U01{}'.format(i)))
        else:
            revoke_user_privileges('U01{}'.format(i))

    for time in times:
        print(time)


def grant_user_privileges(user_id, attribute_num):
    '''
    Measure the time to grant a user with a certain privilege level
    '''
    credentials = None
    attributes = {}

    for x in range(0, attribute_num):
        attributes.update({'attribute{}'.format(x): 'value{}'.format(x)})

    attributes = json.dumps(attributes)
    credentials = [user_id, "doctorA{}".format(user_id), "password1", attributes]
    
    start_time = time.time()
    db.create_user(*credentials)

    # generate cert time
    # sign attributes
    signature = private_key.sign(
        bytes(attributes, 'utf-8'),
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA256()
    )

    # generate certificate
    cert = {"uid": user_id, "attributes": attributes, "signature": signature.hex()}

    end_time = time.time()

    return(end_time - start_time)


def evaluate_grant_time(repeats = 5):
    '''
    Evaluate the average time to grant a user each privilege level
    '''
    # times shows accumulated times from low to high privilege
    times = []
    attribute_nums = [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100]
    for i in range(repeats):
        for attribute_num in attribute_nums:
            times.append(grant_user_privileges("Ug1{}-{}".format(i, attribute_num), attribute_num))
        

    average_times = [time / repeats for time in times]
    for time in average_times:
        print(time)


if __name__=="__main__":
    # only generate once
    # generate_rsa_key_pair()

    print("\nrevoke")
    evaluate_revocation_time(10)

    print("\ngenerate")
    evaluate_grant_time(repeats = 1)
