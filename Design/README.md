This code implements the essentials of design 2 to allow for the evaluation of the scalability of the design

### SETUP ###
The following requirements must be installed:
1. MySQL 8.0.19 server for the machine running the 'Server' server
2. SQLite for the machines running the 'AA' abd 'KeyServer' servers
3. the python modules in the requirements.txt list. This can be done using 'pip3 install -r requirements.txt'

The MySQL server must be configured with a database called 'patient_data'. The server will handle creating the required tables. The credentials used to access the server must be username 'root' and password 'password'.

The generate_data.sh script must then be run to generate the required test data. This will create the Resources directory heirarchy that stores
the random binary data files.

A port and ip that the server will be available on must be added to the config.py of each server directory. Each server directory has its own config.py file 
to add required configuration information for that server. 

### Run Instructions ###
Each server must then be run in its own terminal by navigating to its root directory e.g for the AA server it would be 'cd AA'. Then the server may be run
using uvicorn by
    'uvicorn main:app --port {}' and the port to run on.