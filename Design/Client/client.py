from Crypto.Cipher import AES 
from secrets import token_bytes
import requests
import hashlib
import struct
import random
import base64
import shutil
import json
import sys
import os
import time

from Client.policies import *
from Client.config import *

# test method
def get_patient_record_list(patient_id):
    '''
    Gets all records for a given patient_id
    '''
    url = "{}/list".format(CLOUD_BASE_URL)
    headers = {'Content-Type': 'application/json'}
    body = {'patient_id':patient_id}

    response = requests.get(url, headers=headers, json=body)
    
    return(response)


def get_record_token(user, patient_id, record_id):
    '''
    Get the encrypted token from the server
    '''
    url = "{}/token".format(CLOUD_BASE_URL)
    headers = {}
    headers.update(user.auth)
    body = {'patient_id': patient_id,
            'record_id': record_id}

    response = requests.get(url, json=body, headers=headers)

    return(response)


def decrypt_token(private_key, encrypted_token):
    '''
    CP-ABE not implemented so return 'encrypted token' as unencrypted token
    '''
    encrypted_bytes = bytes.fromhex(encrypted_token)

    # decrypt here
    decrypted_token = encrypted_bytes

    token = decrypted_token.hex()
    return(token)


def hash_token(token):
    '''
    Hash the token to use to request an encrypted file
    '''
    m = hashlib.sha256()
    m.update(bytes.fromhex(token))
    hashed_token = m.hexdigest()

    return(hashed_token)


def get_patient_record(user, patient_id, record_id, decrypt=True, print_times=False):
    '''
    Fetch the patient record and DEK and decrypt unless specified not to
    '''
    # try:
    start_time = time.time()

    # fetch and decrypt encrypted token
    client_start = time.time()
    token_response = get_record_token(user, patient_id, record_id)
    tr_json = json.loads(token_response.text)
    encrypted_token = tr_json['encrypted_token']
    server_time = tr_json['time']
    token = decrypt_token("placeholder key", encrypted_token)
    hashed_token = hash_token(token)
    client_time = time.time() - client_start

    # fetch encrypted record
    transfer_start = time.time()
    file_path, s_time = download_file(user, patient_id, record_id, hashed_token)
    transfer_time = time.time() - transfer_start - s_time
    server_time += s_time
    
    # fetch DEK
    response = request_key(user, patient_id, record_id, token, user.certificate)
    r_json = json.loads(response.text)
    if (response.status_code == 403):
        print(response.reason)
        return
    key = r_json['key']
    ta_time = r_json['time']
    
    # decrypt record using DEK
    client_start = time.time()
    if(decrypt):
        decrypt_record(file_path, bytes.fromhex(key))
    client_time += time.time() - client_start

    total_time = time.time() - start_time
    communication_overheads = total_time - client_time - server_time - ta_time - transfer_time

    if(print_times):
        print("{} {} {} {} {} {}".format(total_time, client_time, server_time, ta_time, transfer_time, communication_overheads))
    # except Exception as err:
    #     print("ERROR: Failed to download and/or decrypt patient record due to, {}".format(err))
    

def store_patient_record(user, patient_id, record_type, desc, file_name):
    '''
    Upload the patient record to the cloud storage server

    file should be encrypted before uploading
    '''
    url = "{}/record".format(CLOUD_BASE_URL)
    headers = {}
    headers.update(user.auth)
    body = {'patient_id': patient_id,
            'record_type': record_type,
            'desc': desc}

    enc_file_name = "{}-encrypted".format(file_name)
    with open(enc_file_name, "rb") as f:
        # t1 = time.time()
        response = requests.post(url, files={'file': f}, data=body, headers=headers)
        # t2 = time.time()
        # print(t2 - t1)

    # if response is 201 then delete the encrypted file
    return(response)


def upload_record(user, patient_id, record_type, desc, file_name, policy_rule, print_times=False, return_token = False):
    '''
    Handles the encryption and upload of the file and DEK
    '''
    try:
        start_time = time.time()
        
        client_start = time.time()
        key = encrypt_record(file_name)[0]
        client_time = time.time() - client_start
 
        transfer_start = time.time()
        response = store_patient_record(user, patient_id, record_type, desc, file_name)
        transfer_time = time.time() - transfer_start
        response.raise_for_status()
        r_json = json.loads(response.text)
        record_id = r_json['record_id']
        server_time = r_json['time']
        transfer_time -= server_time

        client_start = time.time()
        policy = create_policy("X{}{}".format(patient_id, record_id), "desc", policy_rule, record_id)
        client_time += time.time() - client_start

        response_2 = save_key(user, patient_id, record_id, key.hex(), json.dumps(policy))
        times = json.loads(response_2.text)['time']
        ta_time = times[0]
        server_time += times[1]
        token = json.loads(response_2.text)['token']
        
        total_time = time.time() - start_time
        communication_overheads = total_time - client_time - server_time - ta_time - transfer_time
        if(print_times):
            print("{} {} {} {} {} {}".format(total_time, client_time, server_time, ta_time, transfer_time, communication_overheads))

        if (return_token):
            return(record_id, token)
        else:
            return(record_id)
    except Exception as err:
        print("ERROR: {}".format(err))
        print("Aborting transaction")
        # try and abort the individual requests


def generate_keys():
    '''
    Generate keys and then upload
    '''
    # generate secret 16 byte (128-bit) key (keep secret)
    key = token_bytes(16)

    # generate IV 
    iv = token_bytes(16)

    return key, iv


def encrypt_record(file_name):
    key, iv = generate_keys()
    aes = AES.new(key, AES.MODE_CBC, iv)

    # try to create a new encrypted file with the data size and iv at the start
    try:
        f_out = open("{}-encrypted".format(file_name), 'wb')
    except Exception as err:
        print("Failed to create encrypted file, file name already in use, {}".format(err))
        sys.exit(-1)

    with open(file_name, "rb") as f_in:
        # write data size
        file_size = os.path.getsize(file_name)
        f_out.write(struct.pack("<Q", file_size))

        # write iv
        f_out.write(iv)

        # write the encrypted data in chunks
        while True:
            data = f_in.read(1024)
            data_size = len(data)
            if(data_size == 0):
                # reached EOF
                break
            elif(data_size % 16 != 0):
                # data is not a multiple of 16 bytes in length so pad with spaces
                data += b' ' * (16 - data_size % 16) 
            ciphertext = aes.encrypt(data)
            f_out.write(ciphertext)

    return(key, iv)


def decrypt_record(file_name, key: bytearray):
    # try to create a new unencrypted file
    try:
        f_out = open(file_name, 'wb')
    except Exception as err:
        print("Failed to create file, file name already in use, {}".format(err))
        sys.exit(-1)

    # read iv and file_size from file
    with open("{}-encrypted".format(file_name), "rb") as f_in:
        # read data size
        file_size = struct.unpack("<Q", f_in.read(struct.calcsize("<Q")))[0]

        # read iv
        iv = f_in.read(16)

        aes = AES.new(key, AES.MODE_CBC, iv)

        # write the decrypted data in chunks
        while True:
            data = f_in.read(1024)
            data_size = len(data)
            if(data_size == 0):
                # reached EOF
                break

            plaintext = aes.decrypt(data)

            if(file_size >= data_size):
                # write entire chunk to file
                f_out.write(plaintext)
            else:
                # remove padding at the end of the file
                plaintext = plaintext[:file_size]

            file_size -= data_size
 

def download_file(user, patient_id, record_id, hashed_token):
    '''
    Download the encrypted file from the cloud server
    '''
    url = "{}/record".format(CLOUD_BASE_URL)
    headers = {}
    headers.update(user.auth)
    body = {'patient_id': patient_id,
            'record_id': record_id,
            'hashed_token': hashed_token}

    local_filename = "{}-{}".format(patient_id, record_id)
    client_dir = os.path.dirname(os.path.abspath(__file__))
    file_path = os.path.join(client_dir, "Files", local_filename)

    computation_time = None
    with requests.get(url, json=body, stream=True, headers=headers) as r:
        computation_time = float(r.headers['time'])
        with open("{}-encrypted".format(file_path), 'wb') as f:
            shutil.copyfileobj(r.raw, f)

    return(file_path, computation_time)


def request_key(user, patient_id, record_id, token, certificate):
    '''
    Request the DEK from the key server
    '''
    url = "{}/key".format(KEY_SERVER_BASE_URL)
    headers = {'Content-Type': 'application/json'}
    headers.update(user.auth)
    body = {'patient_id': patient_id,
            'record_id': record_id,
            'token': str(token),
            'certificate': certificate}

    response = requests.get(url, headers=headers, json=body)
    
    return(response)


def get_dek(user, patient_id, record_id, token):
    '''
    Test wrapper function for reusablility of evaluation functions
    '''
    start_time = time.time()
    request_key(user, patient_id, record_id, token, user.certificate)
    
    return(time.time() - start_time)


def save_key(user, patient_id, record_id, key, policy):
    url = "{}/key".format(KEY_SERVER_BASE_URL)
    headers = {'Content-Type': 'application/json'}
    headers.update(user.auth)
    body = {'patient_id': patient_id,
            'record_id': record_id,
            'key': str(key),
            'policy': str(policy)}

    response = requests.post(url, headers=headers, json=body)
    
    return(response)


def request_cert(user):
    '''
    Request the user's certificate for attributes from the AA
    '''
    url = "{}/cert".format(AA_BASE_URL)
    headers = {}
    headers.update(user.auth)

    response = requests.get(url, headers=headers)
    
    return(response)


def save_user_certificate(user):
    response = request_cert(user)
    cert = json.loads(response.text)
    user.certificate = cert


# test method
def get_all_keys():
    url = "{}/keys".format(KEY_SERVER_BASE_URL)

    response = requests.get(url)
    
    return(response.text)


def main():
    from user import User


    user1 = User('doctor1', 'password1', 'doctor')
    save_user_certificate(user1)

    upload_record(  user1, 
                    "P001", 
                    "XRay", 
                    "Summary care record for patient 123 5MB", 
                    str(os.path.join( os.getcwd(), 'Resources/Evaluation/Bytes',  'bytes1MB')), 
                    COMPLEX_RULE,
                    print_times=True
                )

    print("\nPatient list")
    print(get_patient_record_list("P001").text)

    print("\nkeys")
    print(get_all_keys())
    # print(request_key(user1, "P001", 1).text)
    # print(request_key(patient1, "P001", 1).text)
    # print(request_key(patient2, "P001", 1).text)

    print("\nRecord")
    get_patient_record(user1, "P001", 1, print_times=True)


if __name__=="__main__":
    main()