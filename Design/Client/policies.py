
def create_policy(policy_id, description, rules, record_id):
    '''
    Create a JSON format access policy using supplied data
    '''
    policy_json = {
        "uid": str(policy_id),
        "description": str(description),
        "effect": "allow",
        "rules": rules,
        "targets": {
            "subject_id": "*",
            "resource_id": str(record_id),
            "action_id": "*"
        },
        "priority": 1
    }

    return(policy_json)

### EXAMPLE POLICIES ###

FRIENDS_ONLY_RULE = {
        "subject": [{"$.uid": {"condition": "Equals", "value": "U1000"}},
                    {"$.uid": {"condition": "Equals", "value": "U1001"}}],
        "resource": {},
        "action": [{"$.method": {"condition": "Equals", "value": "get"}}],
        "context": {}
    }

FEMALE_DOCTORS_ONLY_RULE = {
        "subject": {"$.role": {"condition": "Equals", "value": "doctor"},
                    "$.gender": {"condition": "Equals", "value": "female"}},
        "resource": {},
        "action": [{"$.method": {"condition": "Equals", "value": "get"}}],
        "context": {}
    }

EXPERIENCED_NURSES_ONLY_RULE = {
        "subject": {"$.role": {"condition": "Equals", "value": "nurse"},
                    "$.years_experience": {"condition": "Gt", "value": "5"}},
        "resource": {},
        "action": [{"$.method": {"condition": "Equals", "value": "get"}}],
        "context": {}
    }

COMPLEX_RULE = {
        "subject":  [   

                        {
                            "$.role": {"condition": "Equals", "value": "paramedic"},
                            "$.emergency": {"condition": "Equals", "value": "true"}
                        },
                        {
                            "$.role": {"condition": "Equals", "value": "doctor"},
                            "$.hospital": {"condition": "Equals", "value": "H001"}
                        }
                    ],
        "resource": {},
        "action": [{"$.method": {"condition": "Equals", "value": "get"}}],
        "context": {}
    }