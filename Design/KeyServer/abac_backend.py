from py_abac.storage.base import StorageBase
from py_abac import Policy
import dbHooks
import json

class CustomStorage(StorageBase):
    """
    My custom backend specific storage
    """
    def __init__(self, db: dbHooks):
        self.db = db

    def add(self, policy):
        """
        Store a Policy
        """
        pass

    def get(self, uid):
        """
        Retrieve a Policy by its ID
        """
        pass

    def get_all(self, limit, offset):
        """
        Retrieve all stored Policies (with pagination)
        """
        pass

    def update(self, policy):
        """
        Store an updated Policy
        """
        pass

    def delete(self, uid):
        """
        Delete Policy from storage by its ID
        """
        pass

    def get_for_target(self, subject_id, resource_id, action_id):
        """
        Retrieve Policies that match the given target IDs
        """
        default_deny_policy_json = {
            "uid": "0",
            "description": "Default deny",
            "effect": "deny",
            "rules": {
                "subject": {},
                "resource": {},
                "action": {},
                "context": {}
            },
            "targets": {},
            "priority": 0
        }
        # fetch policy from the sqlite database
        policy = json.loads(self.db.get_patient_policy(resource_id)[0])
        
        return([Policy.from_json(policy), Policy.from_json(default_deny_policy_json)])