from fastapi import FastAPI, Header, HTTPException, File, UploadFile, Form, Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from py_abac import PDP, Policy, AccessRequest, EvaluationAlgorithm
from abac_backend import CustomStorage
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.backends import default_backend
from cryptography.exceptions import InvalidSignature
import hashlib
import secrets
import requests
import time
import json
import os

from config import *
import dbHooks
import models 

app = FastAPI()
db = dbHooks.Database("keys.db")
policy_storage = CustomStorage(db)
pdp = PDP(policy_storage, EvaluationAlgorithm.HIGHEST_PRIORITY)

security = HTTPBasic()

# load public key
key_file = open("public-key", "rb")
public_key = serialization.load_pem_public_key(
    key_file.read(),
    backend=default_backend()
)  
key_file.close()

# test endpoint
@app.get("/keys", status_code=200)
async def list_keys(patient: models.Patient = None):
    key_list = None

    try:
        if (patient and patient.patient_id):
            key_list = db.get_patient_key_list(patient.patient_id)
        else:
            key_list = db.get_key_list()
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="error on retrieving key list")

    # convert key list to JSON format
    json_keys = []
    try:
        if(key_list != None):
            json_keys = [{"key_id":key[0], "patient_id": key[1], "record_id": key[2], "key": key[3], "token": key[4]} for key in key_list]
    except KeyError as k_err:
        print("ERROR: Failed to parse key,\n{}".format(k_err))
        raise HTTPException(status_code=500, detail="Error on retrieving key list")
    json_key_list = {"keys": json_keys}
    return(json_key_list)


@app.get("/key", status_code=200)
async def get_key(request: models.Key_Request, credentials: HTTPBasicCredentials = Depends(security)):
    start_time = time.time()
    user_id, role = authenticate_user(credentials)

    # check token validity
    valid_token = check_token(request.token, request.patient_id, request.record_id)
    if not(valid_token):
        print("invalid token")
        raise HTTPException(status_code=403, detail="Permission Denied")
    
    # check certificate validity
    json_cert = json.loads(request.certificate)
    try:
        public_key.verify(
            bytes.fromhex(json_cert['signature']),
            bytes(json_cert['attributes'], 'utf-8'),
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )
    except InvalidSignature:
        raise HTTPException(status_code=403, detail="Permission Denied")

    if not(json_cert['uid'] == user_id):
        raise HTTPException(status_code=403, detail="Permission Denied")

    # check against revoked list

    # read certificate attributes and create a request json object
    attributes = json.loads(json_cert['attributes'])
    request_json = {
        "subject": {
            "id": "", 
            "attributes": attributes
        },
        "resource": {
            "id": str(request.record_id)
        },
        "action": {
            "id": "", 
            "attributes": {"method": "get"}
        },
        "context": {}
    }

    # check request against access policy
    access_request = AccessRequest.from_json(request_json)
    if not(pdp.is_allowed(access_request)):
        print("denied access")
        raise HTTPException(status_code=403, detail="Permission Denied")

    # fetch key
    key = None
    try:
        key = db.get_patient_key_policy(request.patient_id, request.record_id)[0]
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="Error retrieving key")
      
    # grant key
    computation_time = time.time() - start_time
    json_key = {'key': key, 'time': computation_time}
    return(json_key)


@app.post("/key", status_code=201)
async def save_key(key: models.Key_Upload, credentials: HTTPBasicCredentials = Depends(security)):
    start_time = time.time()
    
    user_id, role = authenticate_user(credentials)

    # authorize user
    # only patients and providers should be able to add keys and data to the system,
    # providers may not select a policy on the key as this is done automatically

    # save token to cloud server
    computation_time = None
    try:
        token, encrypted_token, hashed_token = gen_token()
       
        db.create_key(key.patient_id, key.record_id, key.key, token, key.policy)

        computation_time = time.time() - start_time
        response = save_token(key.patient_id, key.record_id, encrypted_token, hashed_token)
        response.raise_for_status()
        server_time = json.loads(response.text)['time']
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="failed to save key")

    return {"message": "key created", "time": [computation_time, server_time], 'token': token}


@app.put("/admin/revoke", status_code=200)
async def revoke_cert(user: models.Revoked_User):

    hash = json.loads(user.revoked_cert)['attribute_hash']

    # save revoked cert to database
    db.revoke_user(user.user_id, hash, "some time...")

    return {"message": "revoked"}


def authenticate_user(credentials: HTTPBasicCredentials):
    '''
    Authenticate the user using HTTP Basic Auth
    '''
    error = HTTPException(
            status_code = 401,
            detail = "Incorrect username or password"
        )

    # check user credentials
    if(credentials is None):
        raise error

    user = db.get_user(credentials.username)
    password_correct = False

    if(user):
        # check password matches
        correct_password = user[2]
        password_correct = secrets.compare_digest(correct_password, credentials.password)

    if not(user and password_correct):
        raise error

    # return user id and role
    return((user[0], user[3]))


def gen_token():
    '''
    Generate a random token, compute the hash and encrypt using CP-ABE
    '''
    # generate random 16-byte token
    token = secrets.token_bytes(16)
    m = hashlib.sha256()
    m.update(token)
    hashed_token = m.hexdigest()

    # a suitable CP-ABE scheme cannot be used due to dependency issues with Charm-Crypto Library
    encrypted_token = token

    return(token.hex(), encrypted_token.hex(), hashed_token)


def save_token(patient_id, record_id, encrypted_token, hashed_token):
    url = "{}/admin/token".format(CLOUD_BASE_URL)
    headers = {'Content-Type': 'application/json'}
    body = {'patient_id': patient_id,
            'record_id': record_id,
            'encrypted_token': encrypted_token,
            'hashed_token': hashed_token}

    response = requests.post(url, headers=headers, json=body)
    
    return(response)


def check_token(token, patient_id, record_id):
    correct_token = db.get_token(patient_id, record_id)[0]
    if(token == correct_token):
        return(True)
    else:
        return(False)
