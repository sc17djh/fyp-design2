from pydantic import BaseModel

'''
models.py stores the request body models for use with fastAPI. models correspond to a model of the input body data supplied to the API endpoint.
'''

class Record(BaseModel):
    patient_id: str
    record_type: str 
    desc: str

class Record_Pair(BaseModel):
    patient_id: str
    record_id: str
    token: str

class Patient(BaseModel):
    patient_id: str = None
    
class Revoked_User(BaseModel):
    user_id: str
    revoked_cert: str

class Key_Upload(BaseModel):
    patient_id: str
    record_id: str
    key: str
    policy: str

class Key_Request(BaseModel):
    patient_id: str
    record_id: str
    token: str
    certificate: str

